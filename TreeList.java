import java.util.ArrayList;

@SuppressWarnings("serial")
public class TreeList extends ArrayList<Tree>
{

	/**
	 * Creates a default TreeList of size 10
	 */
	public TreeList()
	{
		super();
	}
	
	/**
	 * gets the total number of trees in a file
	 * @return the total number of trees
	 */
	public int getTotalNumberOfTrees()
	{
		return this.size();
	}
	
	/**
	 * Searches for and finds the total number of species of a tree in NYC
	 * @param speciesName the name of the species to search for
	 * @return total number of species of a tree
	 */
	public int getCountByTreeSpecies(String speciesName)
	{
		int count = 0;
		for(int i = 0; i < this.size(); i++)
		{
			if(this.get(i).getSpecies().toLowerCase().contains(speciesName.toLowerCase()))
				count++;
		}
		return count;
	}
	
	/**
	 * Finds the number of trees in a certain borough
	 * @param boroName the name of the borough in which to count
	 * @return the number of trees in the specified borough
	 */
	public int getCountByBorough(String boroName)
	{
		int count = 0;
		for(int i = 0; i < this.size(); i++)
		{
			if(this.get(i).getBoroName().toLowerCase().contains(boroName.toLowerCase()))
				count++;
		}
		return count;
	}
	
	/**
	 * Looks for a species within a borough
	 * @param speciesName The name of the species the method is looking for
	 * @param boroName The borough in which to search
	 * @return the number of trees that have the given species name is the borough
	 */
	public int getCountByTreeSpeciesBorough(String speciesName, String boroName)
	{
		int count = 0;
		for(int i = 0; i < this.size(); i++)
		{
			if(this.get(i).getBoroName().toLowerCase().contains(boroName.toLowerCase()) && this.get(i).getSpecies().toLowerCase().contains(speciesName.toLowerCase()))
				count++;
		}
		return count;
	}
	
	/**
	 * Finds all kinds of a certain species name in the TreeList.
	 * @param speciesName the name of the species to search for
	 * @return an ArrayList of all the matching species with no duplicates
	 */
	public ArrayList<String> getMatchingSpecies(String speciesName)
	{
		ArrayList<String> ret = new ArrayList<String>();
		for(int i = 0; i < this.size(); i++)
		{
			if(this.get(i).getSpecies().toLowerCase().contains(speciesName.toLowerCase()) && !(ret.contains(this.get(i).getSpecies().toLowerCase())))
			{
				ret.add(this.get(i).getSpecies().toLowerCase());
			}
		}
		return ret;
	}
	
	@Override
	public String toString()
	{
		String ret = "";
		for(int i = 0; i<this.size(); i++)
		{
			ret += ("\n" + this.get(i).toString());
		}
		return ret;
	}

}
